#include "ZodiacRequest.h"
#include <iostream>

ZodiacRequest::ZodiacRequest() : Request("Zodiac")
{
	std::string	inputDate;
	std::cout << "Insert your birthdate! (mm-dd-yyyy):" << std::endl;
	std::cin >> inputDate;
	this->content.push_back(boost::property_tree::ptree::value_type("date", inputDate));
}