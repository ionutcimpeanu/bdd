#include <iostream>
#include <memory>

#include "Session.h"

int main(int argc, char** argv)
{
	const auto host = "127.0.0.1";
	const auto port = "2751";

	// The io_context is required for all I/O
	boost::asio::io_context ioContext;
	RequestsManager requestsManager;
	auto zodiac = requestsManager.getMap().at("Zodiac")->getContentAsString();
	std::make_shared<Session>(ioContext)->run(host, port, zodiac);

	// Run the I/O service. The call will return when
	// the socket is closed.
	ioContext.run();
	return EXIT_SUCCESS;
}