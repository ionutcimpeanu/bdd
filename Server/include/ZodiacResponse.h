#pragma once

#include "Response.h"
#include"boost/date_time/gregorian/gregorian.hpp"

class ZodiacResponse : public Framework::Response
{
public:
	ZodiacResponse();

	std::string zodiacDate(boost::gregorian::date date); 
	std::string interpretPacket(const boost::property_tree::ptree& packet);
};