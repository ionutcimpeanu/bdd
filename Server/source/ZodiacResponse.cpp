#include "ZodiacResponse.h"
#include <regex>
ZodiacResponse::ZodiacResponse() : Response("Zodiac")
{

}

std::string ZodiacResponse::zodiacDate(boost::gregorian::date date)
{
	std::string astro_sign;
		if (date.month() == 12) {

			if (date.day() < 22)
				astro_sign = "Sagittarius (November 22 - December 21)";
			else
				astro_sign = "Capricorn (December 22 -January 20)";
		}
		
		else if (date.month() == 1) {
			if (date.day() < 20)
				astro_sign = "Capricorn (December 22 -January 20)";
			else
				astro_sign = "Aquarius (January 20 -February 18)";
		}

		else if (date.month() == 2) {
			if (date.day() < 19)
				astro_sign = "Aquarius  (January 20 -February 18)";
			else
				astro_sign = "Pisces (March 15 - April 14)";
		}

		else if (date.month() == 3) {
			if (date.day() < 21)
				astro_sign = "Pisces (March 15 - April 14)";
			else
				astro_sign = "Aries (April 15- May 15)";
		}
		else if (date.month() == 4) {
			if (date.day() < 20)
				astro_sign = "Aries (April 15- May 15)";
			else
				astro_sign = "Taurus (April 20-May 21)";
		}

		else if (date.month() == 5) {
			if (date.day() < 21)
				astro_sign = "Taurus (April 20-May 21)";
			else
				astro_sign = "Gemini (May 21-June 21)";
		}

		else if (date.month() == 6) {
			if (date.day() < 21)
				astro_sign = "Gemini (May 21-June 21)";
			else
				astro_sign = "Cancer (June 22 - July 22)";
		}

		else if (date.month() == 7) {
			if (date.day() < 23)
				astro_sign = "Cancer (June 22 - July 22)";
			else
				astro_sign = "Leo (July 23  - August 22)";
		}

		else if (date.month() == 8) {
			if (date.day() < 23)
				astro_sign = "Leo (July 23  - August 22)";
			else
				astro_sign = "Virgo (August 23  - September 22)";
		}

		else if (date.month() == 9) {
			if (date.day() < 23)
				astro_sign = "Virgo";
			else
				astro_sign = "libra";
		}

		else if (date.month() == 10) {
			if (date.day() < 23)
				astro_sign = "Libra (August 23  - September 22)";
			else
				astro_sign = "Scorpio (October 23  - November 21 )";
		}

		else if (date.month() == 11) {
			if (date.day() < 22)
				astro_sign = "Scorpio (October 23  - November 21 )";
			else
				astro_sign = "Sagittarius (November 22  - December 21 )";
		}

	return astro_sign;
}

std::string ZodiacResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	auto data = packet.get<std::string>("date"); // take the date readed from client
	std::regex expression("(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2}"); //experssion that accept just date of type mm/dd/yyyy
	if (std::regex_match(data, expression)) // check if is a valid date
	{
		auto date2 = boost::gregorian::from_us_string(data); //split day year month...
		this->content.push_back(boost::property_tree::ptree::value_type("File", "Zodiac.txt"));
		this->content.push_back(boost::property_tree::ptree::value_type("Result", zodiacDate(date2))); //sending the result 
		
	
	}
	else
	{
		this->content.push_back(boost::property_tree::ptree::value_type("File", "Zodiac.txt"));
		this->content.push_back(boost::property_tree::ptree::value_type("Result", "data invalida")); // doesn't validate regex expression
		// valid format
	}

	return this->getContentAsString();
}
