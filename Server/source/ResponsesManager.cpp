#include"D:\\Facultate\\TeamWork\\UnitTesting\\Server\\include\\ResponsesManager.h"
#include "D:\\Facultate\\TeamWork\\UnitTesting\\Server\\include\\ZodiacResponse.h"

ResponsesManager::ResponsesManager()
{
	this->Responses.emplace("Zodiac", std::make_shared<ZodiacResponse>());
	//this->Responses.emplace("Handshake", std::make_shared<HandshakeResponse>());
	//this->Responses.emplace("Sum", std::make_shared<SumResponse>());
	//this->Responses.emplace("WordCounter", std::make_shared<WordCountResponse>());
}

std::map<std::string, std::shared_ptr<Framework::Response>> ResponsesManager::getMap() const
{
	return this->Responses;
}
