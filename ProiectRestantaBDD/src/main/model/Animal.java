
public class Animal {

	private int idAnimal;
	
	private String animalName;
	
	private String animalRace;

	public Animal(int idAnimal, String animalName, String animalRace) {
		super();
		this.idAnimal = idAnimal;
		this.animalName = animalName;
		this.animalRace = animalRace;
	}

	public int getIdAnimal() {
		return idAnimal;
	}

	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}

	public String getAnimalName() {
		return animalName;
	}

	public void setAnimalName(String animalName) {
		this.animalName = animalName;
	}

	public String getAnimalRace() {
		return animalRace;
	}

	public void setAnimalRace(String animalRace) {
		this.animalRace = animalRace;
	}

	@Override
	public String toString() {
		return "Animal [idAnimal=" + idAnimal + ", animalName=" + animalName + ", animalRace=" + animalRace + "]";
	}
	
	
}
