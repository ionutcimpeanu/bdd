package helper;

import java.util.function.Consumer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class ReptilesHelper {
	
	private ReptilesHelper() {
		
	}
	
	private static ReptilesHelper instance;
	
	public static ReptilesHelper getInstance() {
		if (instance == null) {
			instance = new ReptilesHelper();
			instance.init();
		}
		
		return instance;
	}
	
	private EntityManagerFactory entityManagerFactory;
	private EntityManager entityManager;

	

	public boolean executeTransaction(Consumer<EntityManager> action) {
		EntityTransaction entityTransaction = entityManager.getTransaction();
		try {
			entityTransaction.begin();
			action.accept(entityManager);
			entityTransaction.commit();
		} catch (RuntimeException e) {
			System.err.println("Transaction error: " + e.getLocalizedMessage());
			entityTransaction.rollback();
			return false;
		}

		return true;
	}

	private boolean init() {
		try {
			entityManagerFactory = Persistence.createEntityManagerFactory("Reptile");
			entityManager = entityManagerFactory.createEntityManager();
		} catch (Exception e) {
			System.err.println("Error at initialing DatabaseManager: " + e.getMessage().toString());
			return false;
		}

		return true;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}
}