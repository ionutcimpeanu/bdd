package helper;

import java.util.function.Consumer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class PatrupedsHelper {
	
	private PatrupedsHelper() {
		
	}
	
	private static PatrupedsHelper instance;
	
	public static PatrupedsHelper getInstance() {
		if (instance == null) {
			instance = new PatrupedsHelper();
			instance.init();
		}
		
		return instance;
	}
	
	private EntityManagerFactory entityManagerFactory;
	private EntityManager entityManager;

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public boolean executeTransaction(Consumer<EntityManager> action) {
		EntityTransaction entityTransaction = entityManager.getTransaction();
		try {
			entityTransaction.begin();
			action.accept(entityManager);
			entityTransaction.commit();
		} catch (RuntimeException e) {
			System.err.println("Transaction error: " + e.getLocalizedMessage());
			entityTransaction.rollback();
			return false;
		}

		return true;
	}

	private boolean init() {
		try {
			entityManagerFactory = Persistence.createEntityManagerFactory("Patrupede");
			entityManager = entityManagerFactory.createEntityManager();
		} catch (Exception e) {
			System.err.println("Error at initialing DatabaseManager: " + e.getMessage().toString());
			return false;
		}

		return true;
	}
}