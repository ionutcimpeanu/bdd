package model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="Pasari")
public class Pasari implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private int idBird;
	
	private String birdName;
	
	private String birdRace;
	
	public Pasari() {
		
	}

	

	public void setIdBird(int idBird) {
		this.idBird = idBird;
	}

	public String getBirdName() {
		return birdName;
	}

	public void setBirdName(String birdName) {
		this.birdName = birdName;
	}

	public String getBirdRace() {
		return birdRace;
	}

	public void setBirdRace(String birdRace) {
		this.birdRace = birdRace;
	}

	public Pasari(int idBird, String birdName, String birdRace) {
		super();
		this.idBird = idBird;
		this.birdName = birdName;
		this.birdRace = birdRace;
	}

	@Override
	public String toString() {
		return "Pasari [idBird=" + idBird + ", birdName=" + birdName + ", birdRace=" + birdRace + "]";
	}

	public int getIdBird() {
		return idBird;
	}
	
	
}