package model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="Patrupede")
public class Patrupeds implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	
	private int idPet;
	
	private String petName;
	
	private String petRace;
	
	public Patrupeds( ) {
		
	}

	public void setIdPet(int idPet) {
		this.idPet = idPet;
	}

	public String getPetName() {
		return petName;
	}

	public void setPetName(String petName) {
		this.petName = petName;
	}

	public String getPetRace() {
		return petRace;
	}

	public void setPetRace(String petRace) {
		this.petRace = petRace;
	}

	public Patrupeds(int idPet, String petName, String petRace) {
		super();
		this.idPet = idPet;
		this.petName = petName;
		this.petRace = petRace;
	}

	@Override
	public String toString() {
		return "Patrupeds [idPet=" + idPet + ", petName=" + petName + ", petRace=" + petRace + "]";
	}

	public int getIdPet() {
		return idPet;
	}
	
	
	
}