package model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="Reptile")
public class Reptiles implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	private int idAnimal;
	
	private String animalName;
	
	private String animalRace;
	
	public Reptiles() {
		
	}


	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}

	public String getAnimalName() {
		return animalName;
	}

	public void setAnimalName(String animalName) {
		this.animalName = animalName;
	}

	public String getAnimalRace() {
		return animalRace;
	}

	public void setAnimalRace(String animalRace) {
		this.animalRace = animalRace;
	}

	public Reptiles(int idAnimal, String animalName, String animalRace) {
		super();
		this.idAnimal = idAnimal;
		this.animalName = animalName;
		this.animalRace = animalRace;
	}

	@Override
	public String toString() {
		return "Reptiles [idAnimal=" + idAnimal + ", animalName=" + animalName + ", animalRace=" + animalRace + "]";
	}

	public int getIdAnimal() {
		return idAnimal;
	}
	
	
	
}