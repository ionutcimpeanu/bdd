package main;

import java.sql.SQLException;
import java.util.Scanner;

import main.Menu;

public class Main {
	public static void main(String[] args) throws SQLException {

		Menu menu = new Menu();

		System.out.println("To view the content of databases:");
		System.out.println("1.Pasari\n2.Patrupede\n3.Reptile\n");
		System.out.println("To create record in databases press:");
		System.out.println("4.Pasari\n5.Patrupede\n6.Reptile\n7.Exit");
		Scanner sc = new Scanner(System.in);
		int choice = sc.nextInt();
		switch (choice) {
		case 1: {
			System.out.println("Pasari database:");
			menu.printBird();
			break;
		}
		case 2: {
			System.out.println("Patruped database:");
			menu.printPatruped();
			break;
		}
		case 3: {
			System.out.println("Reptiles database:");
			menu.printReptile();
			break;
		}
		case 4: {
			menu.pasariOperations();
			break;
		}
		case 5:{
			menu.patrupedOperations();
			break;
		}
		case 6:{
			menu.reptileOperations();
			break;
		}case 7:{
			System.out.println("End of application.");
			return;
		}

		}

		sc.close();
	}
}
