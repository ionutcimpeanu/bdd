package main;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import daoImpl.PasariDao;
import daoImpl.PatrupedsDao;
import daoImpl.ReptilesDao;
import helper.DatabaseHelper;
import helper.PatrupedsHelper;
import helper.ReptilesHelper;
import model.Pasari;
import model.Patrupeds;
import model.Reptiles;

public class Menu {
	public Menu() {

	}

	Scanner scanner = new Scanner(System.in);
	DatabaseHelper dh = DatabaseHelper.getInstance();
	PatrupedsHelper ph = PatrupedsHelper.getInstance();
	ReptilesHelper rh = ReptilesHelper.getInstance();
	PasariDao dao = new PasariDao(dh);
	PatrupedsDao pDao = new PatrupedsDao(ph);
	ReptilesDao rDao = new ReptilesDao(rh);

	public void printBird() {

		List<Pasari> pasari = dao.getAll();

		for (Pasari p : pasari) {
			System.out.println(p.toString());
		}
	}

	public void printPatruped() {
		List<Patrupeds> listPatrupeds = pDao.getAll();
		for (Patrupeds iterator : listPatrupeds) {
			System.out.println(iterator.toString());
		}
	}

	public void printReptile() {
		List<Reptiles> listReptiles = rDao.getAll();

		for (Reptiles r : listReptiles) {
			System.out.println(r.toString());
		}

	}
	
	

	public void patrupedOperations() {
		System.out.println("Chose an option:");
		System.out.println("1.Read'n print\n2.Create\n3.Delete\n4.Update\n5.Exit");
		int nextChoice = scanner.nextInt();
		switch (nextChoice) {
		case 1: {
			List<Patrupeds> listPatrupeds = pDao.getAll();
			for (Patrupeds iterator : listPatrupeds) {
				System.out.println(iterator.toString());
			}
			break;
		}
		case 2: {
			System.out.println("Before Adding...");
			List<Patrupeds> listPatrupeds = pDao.getAll();
			
			
			for (Patrupeds iterator : listPatrupeds) {
				System.out.println(iterator.toString());
			}
			Scanner sc = new Scanner(System.in);
			String inputName, patrupedsRace;
			Patrupeds patruped = new Patrupeds();
			System.out.println("Id:");
			int id = sc.nextInt();
			System.out.println("Patruped's name:");
			inputName = sc.next();
			

			System.out.println("Patruped's race:");
			patrupedsRace = scanner.next();

			patruped.setIdPet(id);
			patruped.setPetName(inputName);
			patruped.setPetRace(patrupedsRace);
			pDao.create(patruped);

			System.out.println("After Adding...");
			List<Patrupeds> listPatrupeds1 = pDao.getAll();
			for (Patrupeds iterator : listPatrupeds1) {
				System.out.println(iterator.toString());
			}
			break;
		}
		case 3: {
			System.out.println("Before Delete...");
			List<Patrupeds> listPatrupeds = pDao.getAll();
			for (Patrupeds iterator : listPatrupeds) {
				System.out.println(iterator.toString());
			}
			System.out.println("Id to delete:");
			int id=scanner.nextInt();
			Optional<Patrupeds> idPatrupedToDelete = pDao.get(id);
			pDao.delete(idPatrupedToDelete.get());

			System.out.println("After Delete...");
			List<Patrupeds> listPatrupeds1 = pDao.getAll();
			for (Patrupeds iterator : listPatrupeds1) {
				System.out.println(iterator.toString());
			}
			break;
		}
		case 4: {
			System.out.println("Before Update...");
			List<Patrupeds> listPatrupeds = pDao.getAll();
			for (Patrupeds iterator : listPatrupeds) {
				System.out.println(iterator.toString());
			}
			System.out.println("Id:");
			int id = scanner.nextInt();
			Optional<Patrupeds> idPatrupedToUpdate = pDao.get(id);
			Patrupeds patruped = new Patrupeds();
			System.out.println("Id:");
			int id1 = scanner.nextInt();
			System.out.println("Name:");
			String patrupedName = scanner.next();
			System.out.println("Race:");
			String patrupedRace = scanner.next();
			patruped.setIdPet(id1);
			patruped.setPetName(patrupedName);
			patruped.setPetRace(patrupedRace);
		
			pDao.update(idPatrupedToUpdate.get(), patruped);
			System.out.println("After Update...");
			List<Patrupeds> listPatrupeds1 = pDao.getAll();
			for (Patrupeds iterator : listPatrupeds1) {
				System.out.println(iterator.toString());
			}
			break;
		}
		case 5: {
			System.out.println("Done.");
			break;
		}
		default: {
			break;
		}
		}

	}

	public void pasariOperations() {
		System.out.println("Chose an option:");
		System.out.println("1.Read'n print\n2.Create\n3.Delete\n4.Update\n5.Exit");
		int nextChoice = scanner.nextInt();
		switch(nextChoice) {
		case 1:{
			List<Pasari> pasari = dao.getAll();

			for (Pasari p : pasari) {
				System.out.println(p.toString());
			}
			break;
		}
		case 2:{
			System.out.println("Before Adding:");
			List<Pasari> pasari = dao.getAll();

			for (Pasari p : pasari) {
				System.out.println(p.toString());
			}
			
			Pasari pasari1 = new Pasari();
			System.out.println("Id:");
			int id = scanner.nextInt();
			pasari1.setIdBird(id);
			System.out.println("Name:");
			String name = scanner.next();
			pasari1.setBirdName(name);
			System.out.println("Race:");
			String race = scanner.next();
			pasari1.setBirdRace(race);
			
			dao.create(pasari1);
			System.out.println("After Adding:");
			List<Pasari> pasari11 = dao.getAll();

			for (Pasari p : pasari11) {
				System.out.println(p.toString());
			}
			
		}
		case 3:{
			System.out.println("Before Delete:");
			List<Pasari> pasari = dao.getAll();

			for (Pasari p : pasari) {
				System.out.println(p.toString());
			}
			System.out.println("Id to delete:");
			int id=scanner.nextInt();
			Optional<Pasari> idToDelete = dao.get(id);
			dao.delete(idToDelete.get());
			System.out.println("After Delete:");
			List<Pasari> pasari1 = dao.getAll();

			for (Pasari p : pasari1) {
				System.out.println(p.toString());
			}
			
		}
		case 4:{
			System.out.println("Before Update:");
			List<Pasari> pasari = dao.getAll();

			for (Pasari p : pasari) {
				System.out.println(p.toString());
			}
			System.out.println("Id to update:");
			int id = scanner.nextInt();
			Pasari pasari1 = new Pasari();
			Optional<Pasari> idToUpdate = dao.get(id);
			System.out.println("id");
			int id1 = scanner.nextInt();
			
			System.out.println("Name:");
			String name = scanner.next();
			
			System.out.println("Race:");
			String race = scanner.next();
			System.out.println("\n");
			pasari1.setIdBird(id);
			pasari1.setBirdName(name);
			pasari1.setBirdRace(race);
			
			dao.update(idToUpdate.get(), pasari1);
			System.out.println("After Update:");
			List<Pasari> pasari11 = dao.getAll();

			for (Pasari p : pasari11) {
				System.out.println(p.toString());
			}
			
			
			
		}
		case 5:{
			System.out.println("Bye");
			return;
		}
		}
		
	}
	public void reptileOperations() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Chose an option:");
		System.out.println("1.Read'n Print\n2.Create\n3.Delete\n4.Update\n5.Exit");
		int choice = sc.nextInt();
		switch(choice) {
		case 1:{
			List<Reptiles>reptileList = rDao.getAll();
			
			for(Reptiles r :reptileList) {
				System.out.println(r.toString());
			}
			break;
		}
		case 2:{
			System.out.println("Before Delete:");
			List<Reptiles>reptileList = rDao.getAll();
			
			for(Reptiles r :reptileList) {
				System.out.println(r.toString());
			}
			
			Reptiles reptile = new Reptiles();
			System.out.println("Id:");
			int id=scanner.nextInt();
			reptile.setIdAnimal(id);
			System.out.println("Reptile's name:");
			String reptilesName = sc.next();
			reptile.setAnimalName(reptilesName);
			System.out.println("Reptiles's race:");
			String reptileRace = scanner.next();
			reptile.setAnimalRace(reptileRace);
			
			rDao.create(reptile);
			System.out.println("After create:");
			List<Reptiles>reptileList1 = rDao.getAll();
			
			for(Reptiles r :reptileList1) {
				System.out.println(r.toString());
			}
			break;
			
		}
		case 3:{
			System.out.println("After Delete:");
			List<Reptiles>reptileList = rDao.getAll();
			
			for(Reptiles r :reptileList) {
				System.out.println(r.toString());
			}
			
			System.out.println("Id to delete:");
			int id = scanner.nextInt();
			Optional<Reptiles>idToDelete = rDao.get(id);
			rDao.delete(idToDelete.get());
			System.out.println("After Delete:");
			List<Reptiles>reptileList1 = rDao.getAll();
			
			for(Reptiles r :reptileList1) {
				System.out.println(r.toString());
			}
			break;
			
		}
		case 4:{
			System.out.println("Before update:");
			List<Reptiles>reptileList = rDao.getAll();
			
			for(Reptiles r :reptileList) {
				System.out.println(r.toString());
			}
			
			System.out.println("Id:");
			int id= scanner.nextInt();
			Optional<Reptiles>idToUpdate = rDao.get(id);
			Reptiles reptile = new Reptiles();
			System.out.println("Id:");
			int id1 = sc.nextInt();
			System.out.println("Reptile's name:");
			String reptileName = sc.next();
			System.out.println("Reptile's race:");
			String reptileRace = sc.next();
			reptile.setAnimalName(reptileName);
			reptile.setAnimalRace(reptileRace);
			reptile.setIdAnimal(id1);
			
			rDao.update(idToUpdate.get(), reptile);
			System.out.println("After Update:");
			List<Reptiles>reptileList1 = rDao.getAll();
			
			for(Reptiles r :reptileList1) {
				System.out.println(r.toString());
			}
			break;
		}
		case 5:{
			System.out.println("End of Application.");
			return;
		}
		}
	}
}
