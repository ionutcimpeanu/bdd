package daoImpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import dao.Dao;
import helper.ReptilesHelper;
import model.Reptiles;

public class ReptilesDao implements Dao<Reptiles> {

	private ReptilesHelper databaseHelper;
	
	public ReptilesDao(ReptilesHelper dh) {
		this.databaseHelper=dh;
	}
	
	@Override
	public Optional<Reptiles> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Reptiles.class, id));
	}

	@Override
	public List<Reptiles> getAll() {
		TypedQuery<Reptiles> query = databaseHelper.getEntityManager().createQuery("SELECT r from Reptiles r",
				Reptiles.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Reptiles t) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(t));
	}

	@Override
	public boolean update(Reptiles old, Reptiles newObj) {
	old.setIdAnimal(newObj.getIdAnimal());
	old.setAnimalName(newObj.getAnimalName());
	old.setAnimalRace(newObj.getAnimalRace());
	return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	
	}

	@Override
	public boolean delete(Reptiles t) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(t));
	}

}
