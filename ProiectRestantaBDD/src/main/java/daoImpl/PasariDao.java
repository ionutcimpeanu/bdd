package daoImpl;
import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import dao.Dao;
import helper.DatabaseHelper;
import model.Pasari;

public class PasariDao implements Dao<Pasari> {
	
	private DatabaseHelper databaseHelper;
	


	public PasariDao(DatabaseHelper dh) {
		this.databaseHelper=dh;
	}

	@Override
	public Optional<Pasari> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Pasari.class, id));
	}

	public List<Pasari> getAll() {
		TypedQuery<Pasari> query = databaseHelper.getEntityManager().createQuery("SELECT p from Pasari p",
				Pasari.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Pasari t) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(t));
		
	}

	@Override
	public boolean update(Pasari old, Pasari newObj) {
		old.setBirdName(newObj.getBirdName());
		old.setBirdRace(newObj.getBirdRace());
		old.setIdBird(newObj.getIdBird());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Pasari t) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(t));
	}

}