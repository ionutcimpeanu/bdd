package daoImpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import dao.Dao;
import helper.DatabaseHelper;
import helper.PatrupedsHelper;
import model.Patrupeds;

public class PatrupedsDao implements Dao<Patrupeds> {

	private PatrupedsHelper databaseHelper;
	
	public PatrupedsDao(PatrupedsHelper dh) {
		this.databaseHelper=dh;
	}
	
	@Override
	public Optional<Patrupeds> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Patrupeds.class, id));
	}

	@Override
	public List<Patrupeds> getAll() {
		TypedQuery<Patrupeds> query = databaseHelper.getEntityManager().createQuery("SELECT p from Patrupeds p",
				Patrupeds.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Patrupeds t) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(t));
	}

	@Override
	public boolean update(Patrupeds old, Patrupeds newObj) {
	old.setPetName(newObj.getPetName());
	old.setPetRace(newObj.getPetRace());
	old.setIdPet(newObj.getIdPet());
	return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Patrupeds t) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(t));
	}

}
